var mongoose = require('mongoose');
var Reserva = require('./reserva');
var bcrypt = require('bcrypt');

const saltRounds = 10;

var Schema = mongoose.Schema;

function validateMail(email) {
    const re = /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/;
    return re.test(email);

}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim : true,
        required: [true, "El nombre es obligatorio"]

    },
    email : {
        type: String,
        trim : true,
        required: [true, "El email es obligatorio"],
        lowercase : true,
        validate : [validateMail, "Por favor verifique el email"],
        match : [/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/]

    },
    password : {
        type: String,
        required : [true, "El password es obligatorio"]
    },
    passwordResetToken : String,
    passwordResetTokenExpires : Date,
    verificado : {
        type: Boolean,
        default : false
    }

});

usuarioSchema.pre('save', function(next) {
    if(this.isModified('password')){
        this.password = bcrypt-hashSync(this.password, saltRounds) 
    }
    next()
});

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password)
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    
    console.log(reserva);

    reserva.save(cb);
};

module.exports = mongoose.model('Usuario', usuarioSchema); 