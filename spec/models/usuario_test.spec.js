var mongoose = require('mongoose');

var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

var server = require('../../bin/www');

describe('Testing Usuarios', function() {

    afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);

                    done();
                });
            });
        });
    });

    describe('Cuando un Usuario reserva una bici...',() => {
        it('la reserva debe existir', (done) => {
            const usuario = new Usuario({nombre: 'Javier'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate()+1);//TODO usar moment
            usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){ //TODO leer lo de promises
                    console.log(reservas[0]);

                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);

                    done();
                });
            });
        });
    });

});