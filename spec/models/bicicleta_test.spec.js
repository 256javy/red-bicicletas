var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");

//beforeEach( () => {console.log("testeando…"); });

describe('Testing Bicicletas', function ()  {
    beforeEach( function (done)  {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error' , console.error.bind(console, "MongoDB connection error"));
        db.once('open', function ()  {
            console.log('Connected to the db...');
            done();
        });

    });
    
    afterEach( function (done)  {
        Bicicleta.deleteMany({}, function (error, success)  {
            if(error) console.log("error en el afterEach: ", error);
            mongoose.disconnect(error); //solución al timeout después de ejecutar 1 test
            done();
        });
    });

    describe('Bicicleta.createInstance', function () {
        it('Crea una instancia de Bicicleta', function () {
            var bici = Bicicleta.createInstance(1, "verde", "urbana" , [-34.5, -54.1]);
            
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicleta.allBicis', function () {
        it('comienza vacía', function (done)  {
            Bicicleta.allBicis( function (error, bicis) { 
                expect(bicis.length).toEqual(0);
                done();
            })
        });
    });

    describe("Bicicleta.add", () => {
        it("agregamos una", (done) => {
            var bici = new Bicicleta({code : 1, color : "verde", modelo: "urbana"});
            Bicicleta.add(bici, function(err, newBici) {
                if(err) console.log(err);
                Bicicleta.allBicis( function (error, bicis) { 
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(bici.code);
                    done();
                })
            });
        })
    });


    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(error, bicis){
                expect(bicis.length).toBe(0);

                var bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(bici, function(err, newBici){
                    if (err) console.log(err);

                    var bici2 = new Bicicleta({code: 2, color: "roja", modelo: "urbana"});
                    Bicicleta.add(bici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici){
                            expect(targetBici.code).toBe(bici.code);
                            expect(targetBici.color).toEqual(bici.color);
                            expect(targetBici.modelo).toEqual(bici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('borrar la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var bici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(bici, function(err, newBici){
                    if (err) console.log(err);

                    Bicicleta.add(bici, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.allBicis(function(err, bicis){
                            expect(bicis.length).toBe(1);
                            Bicicleta.removeByCode(1, function(error, response) {
                                Bicicleta.allBicis(function(err, bicis){
                                    expect(bicis.length).toBe(0);
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });



});



/*
beforeEach(() => {Bicicleta.allBicis = []});


describe("Bicicleta.allBicis", () => {
    it("comienza vacía", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    } )
});

describe("Bicicleta.add", () => {
    it("agregamos una", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var bici = new Bicicleta(1, 'rojo', 'urbana', [-25.390652, -57.329922]);
        Bicicleta.add(bici);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(bici);
    })
});

describe("Bicicleta.findById", () => {
    it("debe devolver la bici con id 1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var bici1 = new Bicicleta(1, 'rojo', 'urbana');
        var bici2 = new Bicicleta(2, 'gris', 'mtb');
        Bicicleta.add(bici1);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(bici1.id);
        expect(targetBici.color).toBe(bici1.color);
        expect(targetBici.modelo).toBe(bici1.modelo);
    })
});

describe("Bicicleta.findById", () => {
    it("eliminar una bici por id", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var bici1 = new Bicicleta(1, 'rojo', 'urbana');
        Bicicleta.add(bici1);
        expect(Bicicleta.allBicis.length).toBe(1);

        Bicicleta.removeById(bici1.id);

        expect(Bicicleta.allBicis.length).toBe(0);

    })
});
*/