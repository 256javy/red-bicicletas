var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

describe("Bicicleta API", () => {
    describe("GET Bicicletas /", () => {
        it("Status 200", () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var bici1 = new Bicicleta(1, 'rojo', 'urbana');
            Bicicleta.add(bici1);

            request.get("http://localhost:3000/api/bicicletas", (error, response, body) => {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe("POST BICICLETAS /create", () => {
        it("Status 200", (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var headers = {"content-type" : "application/json"}
            var bici1 = '{"id" : 1, "color" :  "rojo", "modelo" : "urbana"}';
            
            request.post({
                headers : headers,
                url : "http://localhost:3000/api/bicicletas/create",
                body : bici1
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                done();
            });
        });
    });

    describe("POST BICICLETAS /delete", () => {
        it("Status 200", (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var headers = {"content-type" : "application/json"}
            var bici1 = '{"id" : 1, "color" :  "rojo", "modelo" : "urbana"}';
            var toDelete = '{"id" : 1}';
            
            //primero agregamos la bici para eliminar
            request.post({
                headers : headers,
                url : "http://localhost:3000/api/bicicletas/create",
                body : bici1
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            });

            //eliminamos la bici recién creada
            request.post({
                headers : headers,
                url : "http://localhost:3000/api/bicicletas/delete",
                body : toDelete
            }, (error, response, body) => {
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
            });
            
        });
    });

    describe("POST BICICLETAS /update", () => {
        it("Status 200", (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var headers = {"content-type" : "application/json"}
            var bici1 = '{"id" : 1, "color" :  "rojo", "modelo" : "urbana", "ubicacion" :[-25.390687, -57.327725]}';
            var toUpdate = '{"id" : 1, "color" :  "blanco", "modelo" : "mtb", "ubicacion" : [-25.390652, -57.329922]}';
            
            //primero agregamos la bici para editar
            request.post({
                headers : headers,
                url : "http://localhost:3000/api/bicicletas/create",
                body : bici1
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            });

            //editamos la bici recién creada
            request.post({
                headers : headers,
                url : "http://localhost:3000/api/bicicletas/update",
                body : toUpdate
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("blanco");
                expect(Bicicleta.findById(1).modelo).toBe("mtb");
                expect(Bicicleta.findById(1).ubicacion).toEqual([-25.390652, -57.329922]); //wow interesante el toEqual, propuesto por el mismo stacktrace
                
                done();
            });
            
        });
    });
});

