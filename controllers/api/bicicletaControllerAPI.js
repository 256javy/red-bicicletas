let Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    res.status(200).json({
        bicicletas : Bicicleta.allBicis
    });
}

exports.bicicleta_create = (req, res) => {
    let bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bicicleta.ubicacion = [req.body.lat, req.body.lon];
    Bicicleta.add(bicicleta);

    res.status(200).json({bicicleta});
}

exports.bicicleta_delete = (req, res) => {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = (req, res) => {
    let bicicleta = Bicicleta.findById(req.body.id);
    
    if (req.body.color) {
        bicicleta.color = req.body.color;
    }
    if (req.body.modelo) {
        bicicleta.modelo = req.body.modelo;
    }
    if (req.body.ubicacion) {
        bicicleta.ubicacion = req.body.ubicacion;
    }
    res.status(200).json({bicicleta});
}