var mymap = L.map('main_map').setView([-25.365181, -57.368395], 13);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//TODO mover los ejemplos
var marker = L.marker([-25.391399, -57.347946]).addTo(mymap);

var circle = L.circle([-25.388326, -57.392578], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

var polygon = L.polygon([
    [-25.341435, -57.359877],
    [-25.349813, -57.34417],
    [-25.359973, -57.353268]
]).addTo(mymap);

marker.bindPopup("<b>Hello world!</b><br>I am a popup.");
circle.bindPopup("I am a circle.");
polygon.bindPopup("I am a polygon.");

var popup = L.popup();


function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(mymap);
}

mymap.on('click', onMapClick);


fetch('api/bicicletas')
  .then(response => response.json())
  .then(data => addBicicletasAlMapa(data.bicicletas));


function addBicicletasAlMapa(bicicleta_list) {
    console.log(bicicleta_list)
    for (const bici of bicicleta_list) {
        L.marker(bici.ubicacion, {title : bici.id} ).addTo(mymap);
    }
}